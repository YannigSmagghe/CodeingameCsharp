using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

class Programm
{
    public enum GameTactics
    {
        FirstCellAvailable,
        RayCastMaxDistance,
        RaycastWithCheckPath,
        AStar,
        Dijkstra,
        Killer
    }
    static void Main(string[] args)
    {
        bool initGame = false;
        Grid grid = new Grid();
        Game game = new Game();

        string[] inputs;

        // game loop
        while (true)
        {
            inputs = Console.ReadLine().Split(' ');
            if (!initGame)
            {
                Game.InitGame(inputs, game);
                game.CurrentTactics = GameTactics.AStar;
                initGame = true;
            }

            game.InitPlayers(inputs);
            game.SortPlayers(game);
            // TACTICS

            Debug.Log("current tactics  " + game.CurrentTactics);

            switch (game.CurrentTactics)
            {
                case GameTactics.FirstCellAvailable:
                    FirstCellAvailable.SeekFirstCellAvailable(game);
                    break;
                case GameTactics.RayCastMaxDistance:
                    RayCastMaxDistance.RaycastBasic(game);
                    break;
                case GameTactics.RaycastWithCheckPath:
                    RayCastMaxDistance.RaycastWithCheckPath(game);
                    break;
                case GameTactics.AStar:
                    AStar.Main(game);
                    break;
                case GameTactics.Dijkstra:
                    break;
                case GameTactics.Killer:
                    break;
                default:
                    break;
            }

            string nextDirection = new VectorToDirection(game.NextDirection).StringDirection;
            Console.WriteLine(nextDirection); // A single line with UP, DOWN, LEFT or RIGHT
        }
    }
}


internal class AStar
{
    private string pathString;
    public string PathString { get => pathString; set => pathString = value; }

    private static Cell startCell = new Cell(Grid.CELLNULL.X, Grid.CELLNULL.Y);
    private static Cell endCell = new Cell(Grid.CELLNULL.X, Grid.CELLNULL.Y);

    // Cell yet available
    private static List<Cell> openCellList = new List<Cell>();
    // Cell complete
    private static List<Cell> closedCellList = new List<Cell>();


    public static void Main(Game game)
    {
        openCellList.Clear();
        closedCellList.Clear();
        Grid.ResetAstarValue();
        startCell = game.GetMainPlayer().CellPosition;

        endCell = game.LumicyclesListEnemies[0].CellPosition;
        Cell nextCell = Astar(startCell);
        if (nextCell != null && endCell.AstarPath.Count > 2)
        {
            Debug.LogCell(nextCell, "ASTAR next cell : ");
            Vector2 secure = nextCell.Position - startCell.Position;
            if (VectorToDirection.CheckDirectionAvailable(secure))
            {
                game.NextDirection = nextCell.Position - startCell.Position;
            }
            else
            {
                Debug.Log("Will Crash");
                RayCastMaxDistance.RaycastBasic(game);
            }
            //Debug.LogListCell(startCell.NeighbourList);
        }
        else
        {// Mais en fait là faudrait close la game. On a plus d'espace, on check comment le rentabiliser
            Debug.Log("SWITCH STARTEGY TO RAYCAST 0 CELL");
            RayCastMaxDistance.RaycastBasic(game);
            //game.CurrentTactics = Programm.GameTactics.RaycastWithCheckPath;
        }
    }

    private static Cell Astar(Cell startCell)
    {
        openCellList.Add(startCell);
        startCell.AstarDistance = Vector2.Distance(startCell.Position, endCell.Position);
        //Debug.LogCell(startCell, "player Position");
        Cell currentNode = startCell;
        do
        {
            currentNode = openCellList[0];
            openCellList.Remove(currentNode);
            closedCellList.Add(currentNode);
            //Debug.LogCell(currentNode, "testcell ");
            foreach (Cell neighbour in currentNode.NeighbourList)
            {
                if (neighbour.Position == endCell.Position)
                {
                    Console.Error.WriteLine("PathFind");
                    endCell.AstarPath = currentNode.AstarPath;
                    endCell.AstarPath.Add(currentNode);
                    endCell.AstarPath.Add(neighbour);
                    openCellList.Clear();
                    //Debug.LogListCell(endCell.AstarPath, "path end encell");
                    endCell.AstarPath = Repath(endCell);
                    break;
                }

                Cell neighbourInClosedList = closedCellList.Find(x => x.Position == neighbour.Position);
                if (neighbourInClosedList != null || neighbour.NeighbourList.Count == 0)
                {
                    continue;
                }

                if(neighbour.Owner != -1)
                {
                    continue;
                }

                float tentativeDistance = Vector2.Distance(neighbour.Position, endCell.Position);
                Cell neighbourInOpenList = openCellList.Find(x => x.Position == neighbour.Position);
                if (currentNode.AstarDistance > tentativeDistance || !openCellList.Contains(neighbour))
                {
                    neighbour.AstarDistance = tentativeDistance;
                    if (!neighbour.AstarPath.Contains(currentNode))
                    {
                        neighbour.AstarPath.Add(currentNode);
                    }
                    if (neighbourInOpenList == null)
                    {
                        openCellList.Add(neighbour);
                    }

                }
            }
        } while (openCellList.Count != 0);
        return endCell.AstarPath[1];
    }

    private static List<Cell> Repath(Cell _endCell)
    {
        List<Cell> newPath = new List<Cell>();
        Cell currentCell = _endCell;
        newPath.Add(_endCell);
        Debug.LogListCell(_endCell.AstarPath, "lastpath a la fin");
        do
        {
            Debug.LogCell(currentCell, "for this");
            Debug.LogListCell(currentCell.AstarPath, "into this");
            foreach (Cell cell in currentCell.AstarPath)
            {
                if (cell == startCell)
                {
                    Debug.LogCell(cell, "we got this");
                    newPath.Add(cell);
                    currentCell = startCell;
                    break;
                }
                if (currentCell.NeighbourList.Contains(cell) && cell.AstarPath.Count > 0)
                {
                    Debug.LogCell(cell, "check this");
                    newPath.Add(cell);
                    currentCell = cell;
                    break;
                }
                
            }

            Debug.Log("===========");
        } while (currentCell != startCell);
        newPath.Reverse();
        Debug.LogListCell(newPath, "lastpath reversed ");
        return newPath;
    }
}

internal class FirstCellAvailable
{
    // Get pos player, look around, set next direction frist available
    public static void SeekFirstCellAvailable(Game game)
    {
        Lumicycles mainPlayer = game.GetMainPlayer();
        List<Cell> cellAvailable = Cell.PositionAroundCell(mainPlayer.CellPosition);
        game.NextDirection = cellAvailable[0].Position - mainPlayer.CellPosition.Position;
    }
}

internal class RayCastMaxDistance
{
    static int maxDist = int.MinValue;
    public static int MaxDist { get => maxDist; set => maxDist = value; }

    private int tempDist;
    static Vector2 nextDirection = Grid.CELLNULL;
    public static Vector2 NextDirection { get => nextDirection; set => nextDirection = value; }

    private static List<Cell> savedRaycast = new List<Cell>();

    struct Ray
    {
        private Vector2 direction;
        public Vector2 Direction { get => direction; set => direction = value; }
        private int value;

        private List<Cell> path;
        public int Value { get => value; set => this.value = value; }

        public List<Cell> Path { get => path; set => path = value; }

        public Ray(Vector2 direction, int value)
        {
            this.direction = direction;
            this.value = value;
            this.path = new List<Cell>();
        }

    }

    public static void RaycastBasic(Game game)
    {
        List<Ray> rayList = RaycastAllDirection(game);
        Ray resultRay = GetHightRay(rayList);
        SetNextDirection(game, resultRay.Direction);
    }

    public static void RaycastWithCheckPath(Game game)
    {
        if (savedRaycast.Count == 0)
        {
            RaycastAllDirection(game);
            List<Ray> rayList = RaycastAllDirection(game);
            Ray resultRay = GetHightRay(rayList);
            savedRaycast = resultRay.Path;

            // Remove player position
            savedRaycast.Remove(savedRaycast[0]);
            Vector2 nextDirection = savedRaycast[0].Position - game.GetMainPlayer().CellPosition.Position;
            savedRaycast.Remove(savedRaycast[0]);
            SetNextDirection(game, nextDirection);
        }
        else
        {
            ContinueAndCheckPath(game);
        }
    }

    private static void ContinueAndCheckPath(Game game)
    {
        if (PathIsSafe())
        {
            Vector2 nextDirection = savedRaycast[0].Position - game.GetMainPlayer().CellPosition.Position;
            savedRaycast.Remove(savedRaycast[0]);
            SetNextDirection(game, nextDirection);
        }
        else
        {
            Debug.Log("PATH NOT SAFE");
            ResetRaycast();
            RaycastWithCheckPath(game);
        }
    }

    private static bool PathIsSafe()
    {
        bool safe = true;
        foreach (Cell cell in savedRaycast)
        {
            if (!Cell.CellIsAvailable(cell))
            {
                safe = false;
                break;
            }
        }

        return safe;
    }
    private static List<Ray> RaycastAllDirection(Game game)
    {
        Lumicycles mainPlayer = game.GetMainPlayer();
        ResetRaycast();


        Ray rayUp = RayCast(mainPlayer.CellPosition, new Ray(Grid.UP, 0));
        Ray rayDown = RayCast(mainPlayer.CellPosition, new Ray(Grid.DOWN, 0));
        Ray rayLeft = RayCast(mainPlayer.CellPosition, new Ray(Grid.LEFT, 0));
        Ray rayRight = RayCast(mainPlayer.CellPosition, new Ray(Grid.RIGHT, 0));

        List<Ray> rayList = new List<Ray>();
        rayList.Add(rayUp);
        rayList.Add(rayDown);
        rayList.Add(rayLeft);
        rayList.Add(rayRight);
        return rayList;
    }

    private static void SetNextDirection(Game game, Vector2 direction)
    {
        game.NextDirection = direction;
    }



    private static Ray GetHightRay(List<Ray> rayList)
    {
        Ray reslutRay = new Ray(Grid.CELLNULL, 0);
        for (int i = 0; i < rayList.Count; i++)
        {
            if (rayList[i].Value > maxDist)
            {
                maxDist = rayList[i].Value;
                reslutRay = rayList[i];
            }
        }
        return reslutRay;
    }

    private static void ResetRaycast()
    {
        MaxDist = 0;
        nextDirection = Grid.CELLNULL;
        savedRaycast.Clear();
    }

    private static Ray RayCast(Cell cell, Ray ray)
    {
        if (Cell.CellIsAvailable(Cell.GetCellWithPosition(cell.Position + ray.Direction)))
        {
            ray.Value = ray.Path.Count;
            ray.Path.Add(cell);
            return RayCast(Cell.GetCellWithPosition(cell.Position + ray.Direction), ray);
        }
        else
        {
            ray.Value = ray.Path.Count;
            ray.Path.Add(cell);
            return ray;
        }

    }
}

internal class Game
{

    private int numberOfPlayer = 0;
    public int NumberOfPlayer { get => numberOfPlayer; set => numberOfPlayer = value; }

    private int mainPlayerNumber;
    public int MainPlayerNumber { get => mainPlayerNumber; set => mainPlayerNumber = value; }

    private List<Lumicycles> lumicyclesList = new List<Lumicycles>();
    internal List<Lumicycles> LumicyclesList { get => lumicyclesList; set => lumicyclesList = value; }

    private List<Lumicycles> lumicyclesListEnemies = new List<Lumicycles>();
    internal List<Lumicycles> LumicyclesListEnemies { get => lumicyclesListEnemies; set => lumicyclesListEnemies = value; }

    private Vector2 nextDirection = Grid.CELLNULL;
    public Vector2 NextDirection { get => nextDirection; set => nextDirection = value; }
    public Programm.GameTactics CurrentTactics { get; internal set; }

    internal void InitPlayers(string[] inputs)
    {
        LumicyclesList.Clear();
        LumicyclesListEnemies.Clear();
        for (int i = 0; i < NumberOfPlayer; i++)
        {
            inputs = Console.ReadLine().Split(' ');
            int X0 = int.Parse(inputs[0]); // starting X coordinate of lightcycle (or -1) // POINT DE DEPART NE CHANGE PAS
            int Y0 = int.Parse(inputs[1]); // starting Y coordinate of lightcycle (or -1)

            Grid.CellMap[Cell.GetCellIndexOnCellMap(new Vector2(X0, Y0))].Owner = i;
            int X1 = int.Parse(inputs[2]); // starting X coordinate of lightcycle (can be the same as X0 if you play before this player) ) // Nouvelle Postion
            int Y1 = int.Parse(inputs[3]); // starting Y coordinate of lightcycle (can be the same as Y0 if you play before this player)


            Grid.CellMap[Cell.GetCellIndexOnCellMap(new Vector2(X1, Y1))].Owner = i;
            LumicyclesList.Add(new Lumicycles(Grid.CellMap[Cell.GetCellIndexOnCellMap(new Vector2(X1, Y1))], i));
            //Debug.LogCell(Cell.GetCell(new Vector2(X1, Y1)));
        }
    }

    public Lumicycles GetMainPlayer()
    {
        Lumicycles resultLumi = null;
        foreach (Lumicycles lumi in LumicyclesList)
        {
            if (lumi.NumberPlayer == MainPlayerNumber)
            {
                resultLumi = lumi;
                break;
            }
        }
        if (resultLumi == null)
        {
            Debug.Log("NO LUMI FOR MAIN PLAYER");
        }
        return resultLumi;
    }

    internal static void InitGame(string[] inputs, Game game)
    {
        game.NumberOfPlayer = int.Parse(inputs[0]); // total number of players (2 to 4).
        game.MainPlayerNumber = int.Parse(inputs[1]); // your player number (0 to 3).
    }

    internal void SortPlayers(Game game)
    {
        foreach (Lumicycles lumi in LumicyclesList)
        {
            if (lumi.NumberPlayer != game.MainPlayerNumber)
            {
                Debug.Log("enemies pos " + lumi.CellPosition.Position);
                LumicyclesListEnemies.Add(lumi);
            }
        }
    }
}

internal class Lumicycles
{
    private Cell cellPosition;
    public Cell CellPosition { get => cellPosition; set => cellPosition = value; }

    private int numberPlayer;
    public int NumberPlayer { get => numberPlayer; set => numberPlayer = value; }

    private float percentOfMap;

    public Lumicycles(Cell currentPosition, int numberPlayer)
    {
        CellPosition = currentPosition;
        NumberPlayer = numberPlayer;
    }

}

internal class Grid
{
    const int mapHeight = 30;
    const int mapWidth = 20;
    public static Vector2 UP = new Vector2(0, -1);
    public static Vector2 DOWN = new Vector2(0, 1);
    public static Vector2 LEFT = new Vector2(-1, 0);
    public static Vector2 RIGHT = new Vector2(1, 0);
    public static Vector2 CELLNULL = new Vector2(-1, -1);
    public static Vector2[] ALLDIRECTION = new Vector2[] { UP, DOWN, LEFT, RIGHT };

    // Variables
    private static List<Cell> cellMap = new List<Cell>();
    internal static List<Cell> CellMap { get => cellMap; set => cellMap = value; }

    // Contructor
    public Grid()
    {
        CellMap = GenerateGrid();
        GenerateNeighbour();
    }

    // Method
    public List<Cell> GenerateGrid()
    {
        List<Cell> _cellMap = new List<Cell>();
        for (int i = 0; i < mapHeight; i++)
        {
            for (int j = 0; j < mapWidth; j++)
            {
                _cellMap.Add(new Cell(i, j));
            }
        }
        //Debug.LogMap(_cellMap);

        return _cellMap;
    }

    public void GenerateNeighbour()
    {
        for (int k = 0; k < cellMap.Count; k++)
        {
            CreateNeighbour(cellMap[k]);
        }
    }

    internal void CreateNeighbour(Cell cell)
    {
        foreach (Vector2 dir in Grid.ALLDIRECTION)
        {
            Vector2 pos = new Vector2(cell.Position.X + dir.X, cell.Position.Y + dir.Y);
            if (Cell.CellIsAvailable(Cell.GetCellWithPosition(pos)))
            {
                cell.NeighbourList.Add(Cell.GetCellWithPosition(pos));
            }
        }
    }

    internal static void ResetAstarValue()
    {
        for (int k = 0; k < cellMap.Count; k++)
        {
            cellMap[k].AstarDistance = 0;
            cellMap[k].AstarPath.Clear();
        }
    }
}


public class Cell
{
    private Vector2 position = new Vector2();
    public Vector2 Position { get => position; set => position = value; }

    private float x;
    public float X { get => x; set => x = value; }

    private float y;
    public float Y { get => y; set => y = value; }

    private int owner = -1;
    public int Owner { get => owner; set => owner = value; }

    private List<Cell> neighbourList = new List<Cell>();
    public List<Cell> NeighbourList { get => neighbourList; set => neighbourList = value; }

    private float astarDistance = float.NegativeInfinity;
    public float AstarDistance { get => astarDistance; set => astarDistance = value; }

    private float astarCout = float.PositiveInfinity;
    public float AstarCout { get => astarCout; set => astarCout = value; }

    private List<Cell> astarPath = new List<Cell>();
    public List<Cell> AstarPath { get => astarPath; set => astarPath = value; }

    public Cell(float _x, float _y)
    {
        x = _x;
        y = _y;
        Position = new Vector2(x, y);
    }

    public static Cell GetCellWithPosition(Vector2 position)
    {
        Vector2 searchValue = position;
        Cell resultCell = new Cell(Grid.CELLNULL.X, Grid.CELLNULL.Y);
        foreach (Cell cell in Grid.CellMap)
        {
            if (cell.Position == searchValue)
            {
                resultCell = cell;
                break;
            }
        }
        return resultCell;
    }


    public static int GetCellIndexOnCellMap(Vector2 position)
    {
        Vector2 searchValue = position;
        int index = -1;
        for (int i = 0; i < Grid.CellMap.Count; i++)
        {
            if (Grid.CellMap[i].Position == searchValue)
            {
                index = i;
                break;
            }
        }
        return index;
    }

    public static List<Cell> PositionAroundCell(Cell cellCheck)
    {
        //Debug.LogCell(cellCheck, "Cell to check");
        List<Cell> cellAvailable = new List<Cell>();
        Vector2 initPosition = cellCheck.Position;
        Vector2 positionUp = initPosition + Grid.UP;
        if (CellIsAvailable(Cell.GetCellWithPosition(positionUp)))
        {
            cellAvailable.Add(GetCellWithPosition(positionUp));
        }
        Vector2 positionDown = initPosition + Grid.DOWN;
        if (CellIsAvailable(GetCellWithPosition(positionDown)))
        {
            cellAvailable.Add(GetCellWithPosition(positionDown));
        }

        Vector2 positionLeft = initPosition + Grid.LEFT;
        if (CellIsAvailable(GetCellWithPosition(positionLeft)))
        {
            cellAvailable.Add(GetCellWithPosition(positionLeft));
        }

        Vector2 positionRight = initPosition + Grid.RIGHT;
        if (CellIsAvailable(GetCellWithPosition(positionRight)))
        {
            cellAvailable.Add(GetCellWithPosition(positionRight));
        }

        //foreach (Cell cell in cellAvailable)
        //{
        //    Debug.Log("cell name " + cell.Position);
        //}
        return cellAvailable;
    }

    public static bool CellIsAvailable(Cell cell)
    {
        if (cell.Position == Grid.CELLNULL)
        {
            return false;
        }
        if (cell.Owner == -1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static bool CellIsAvailableByPosition(Vector2 position)
    {
        return CellIsAvailable(GetCellWithPosition(position));
    }

    public override bool Equals(object obj)
    {
        return obj is Cell cell &&
               Position.Equals(cell.Position);
    }

}

public class VectorToDirection
{
    private Vector2 vectorDirection;
    private string stringDirection;
    public string StringDirection { get => stringDirection; set => stringDirection = value; }

    private enum DirectionAvailable
    {
        LEFT,
        RIGHT,
        UP,
        DOWN
    }

    public VectorToDirection(Vector2 _vectorDirection)
    {
        stringDirection = ConvertVectorToString(_vectorDirection);
    }

    public static bool CheckDirectionAvailable(Vector2 _vectorDirection)
    {
        bool ok = false;
        if (_vectorDirection == Grid.UP)
        {
            ok = true;
        }
        else if (_vectorDirection == Grid.DOWN)
        {
            ok = true;
        }
        else if (_vectorDirection == Grid.LEFT)
        {
            ok = true;
        }
        else if (_vectorDirection == Grid.RIGHT)
        {
            ok = true;
        }
        return ok;
    }

    private string ConvertVectorToString(Vector2 vectorDirection)
    {
        string newDirection = "nodirection";
        if (vectorDirection == Grid.UP)
        {
            newDirection = DirectionAvailable.UP.ToString();
        }
        else if (vectorDirection == Grid.DOWN)
        {
            newDirection = DirectionAvailable.DOWN.ToString();
        }
        else if (vectorDirection == Grid.LEFT)
        {
            newDirection = DirectionAvailable.LEFT.ToString();
        }
        else if (vectorDirection == Grid.RIGHT)
        {
            newDirection = DirectionAvailable.RIGHT.ToString();
        }
        return newDirection;
    }
}


public static class Debug
{
    public static void Log(string log)
    {
        Console.Error.WriteLine(log);
    }

    public static void LogMap(List<Cell> cellMap)
    {
        for (int i = 0; i < cellMap.Count; i++)
        {
            Debug.LogCell(cellMap[i]);
        }
    }

    public static void LogCell(Cell cell, string log = "")
    {
        if (cell != null)
        {
            Console.Error.WriteLine(log + "cell x " + cell.X + " y " + cell.Y + " Owner " + cell.Owner + " Neighbour Count " + cell.NeighbourList.Count + " AstarDistance " + cell.AstarDistance + " ACout " + cell.AstarCout);
        }
        else
        {
            Debug.Log("CELL NULL");
        }
    }

    public static void LogListCell(List<Cell> listCell, string log = "")
    {
        if (listCell != null)
        {
            foreach (Cell cell in listCell)
            {
                Debug.LogCell(cell, log);
            }
        }
        else
        {
            Debug.Log("CELL NULL");
        }
    }

}
