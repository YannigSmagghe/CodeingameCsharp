using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;

/**
 * Grab Snaffles and try to throw them through the opponent's goal!
 * Move towards a Snaffle and use your team id to determine where you need to throw it.
 **/
class Player
{

    public static int HeightMap = 16001;
    public static int WidthMap = 7501;
    public static int NumberWizards = 0;
    public static int NumberSnaffle = 0;
    public static int NumberBludgers = 0;
    public static int TotalEntities = 0;
    public static Game game;
    public static bool gameSetup;

    public static string[] inputsPlayerScore;
    public static string[] inputsOpponentScore;
    public static string[][] inputsEntities;

    static void Main(string[] args)
    {
        StartGame();
    }

    public static void Gameplay()
    {
        StrategyManager();
        InstructionsWizards();
        //Debug.DebugInputsWizards();
        //Debug.DebugInputsSnafle();
    }

    private static void StrategyManager()
    {

        //Strategy.BaseStrat();

        // First Snaffle Free:
        Strategy.FirstAndFree();
    }

    public static class Debug
    {
        public static void DebugInputs()
        {
            for (int i = 0; i < inputsEntities.Length; i++)
            {
                Debug.Log("===== Entities " + i + " ======");
                for (int j = 0; j < 7; j++)
                {
                    // Properties of entities
                    Debug.Log(inputsEntities[i][j]);
                }
                Debug.Log("===============================");
            }
        }
        public static void DebugInputsSnafle()
        {
            for (int i = 0; i < game.Snaffles.Length; i++)
            {
                Debug.Log("Snaffle " + game.Snaffles[i].Position.ToString());
            }
            Debug.Log("Snaffle Lenght " + game.Snaffles.Length.ToString());
        }

        public static void DebugInputsBludgers()
        {
            for (int i = 0; i < game.Snaffles.Length; i++)
            {
                Debug.Log("Snaffle " + game.Snaffles[i].Position.ToString());
            }
            Debug.Log("Snaffle Lenght " + game.Snaffles.Length.ToString());
        }


        public static void DebugInputsWizards()
        {
            for (int i = 0; i < game.Wizards.Length; i++)
            {
                Debug.Log(game.Wizards[i].Position.ToString());
            }
        }

        public static void Log(string log)
        {
            Console.Error.WriteLine(log);
        }
    }

    private static void InstructionsWizards()
    {
        Console.WriteLine(game.Wizards[0].FinalString);
        Console.WriteLine(game.Wizards[1].FinalString);
        //Console.WriteLine("MOVE 0 0 100");
        //Console.WriteLine("MOVE 0 0 100");
    }

    private static void StartGame()
    {

        int myTeamId = int.Parse(Console.ReadLine()); // if 0 you need to score on the right of the map, if 1 you need to score on the left
                                                      //TotalEntities = int.Parse(Console.ReadLine());

        //Console.Error.WriteLine("Debug messages...");
        // game loop
        while (true)
        {

            inputsPlayerScore = Console.ReadLine().Split(' ');
            int myScore = int.Parse(inputsPlayerScore[0]);
            int myMagic = int.Parse(inputsPlayerScore[1]);
            inputsOpponentScore = Console.ReadLine().Split(' ');
            int opponentScore = int.Parse(inputsOpponentScore[0]);
            int opponentMagic = int.Parse(inputsOpponentScore[1]);
            TotalEntities = int.Parse(Console.ReadLine()); // number of entities still in game
            inputsEntities = new string[TotalEntities][];
            for (int i = 0; i < TotalEntities; i++)
            {
                inputsEntities[i] = Console.ReadLine().Split(' ');
                int entityId = int.Parse(inputsEntities[i][0]); // entity identifier
                string entityType = inputsEntities[i][1]; // "WIZARD", "OPPONENT_WIZARD" or "SNAFFLE" (or "BLUDGER" after first league)
                int x = int.Parse(inputsEntities[i][2]); // position
                int y = int.Parse(inputsEntities[i][3]); // position
                int vx = int.Parse(inputsEntities[i][4]); // velocity
                int vy = int.Parse(inputsEntities[i][5]); // velocity
                int state = int.Parse(inputsEntities[i][6]); // 1 if the wizard is holding a Snaffle, 0 otherwise
            }
            if (!gameSetup)
            {
                //gameSetup = true;
                GameSettings.CreateGame();
                GameSettings.SetupTeamId(myTeamId);
                GameSettings.SetupEntities();
            }
            //else
            //{
            //    game.WizardsAlive = 0;
            //    game.SnafflesAlive = 0;
            //    GameSettings.CreateGame();
            //    GameSettings.SetupEntities();
            //    Debug.Log("snaffle alive " + game.Snaffles.Length);
            //    //Debug.DebugInputsSnafle();
            //}

            Gameplay();
        }
    }

    public class Map
    {
        public static Tile[] mapArray = new Tile[HeightMap * WidthMap];

        internal static Tile[] InitializeMap(int height, int width)
        {
            int count = 0;
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    mapArray[count] = new Tile(x, y);
                    count++;
                }
            }

            return mapArray;
        }
    }

    public class Tile
    {
        private double posX;
        private double posY;
        public double PosX { get => posX; set => posX = value; }
        public double PosY { get => posY; set => posY = value; }

        public Tile(double _posX, double _posY)
        {
            PosX = _posX;
            PosY = _posY;
        }


    }

    public class GoalSetup
    {
        public static Vector2 LeftTopGoalLine = new Vector2(0, 1900);
        public static Vector2 LeftCenterGoalLine = new Vector2(0, 3750);
        public static Vector2 LeftBottomGoalLine = new Vector2(0, 5600);
        public static Vector2 RightTopGoalLine = new Vector2(16000, 1900);
        public static Vector2 RightCenterGoalLine = new Vector2(16000, 3750);
        public static Vector2 RightBottomGoalLine = new Vector2(16000, 5600);

        public static Vector2[] RightGoal()
        {
            return new Vector2[3] { RightTopGoalLine, RightCenterGoalLine, RightBottomGoalLine }; ;
        }
        public static Vector2[] LeftGoal()
        {
            return new Vector2[3] { LeftTopGoalLine, LeftCenterGoalLine, LeftBottomGoalLine }; ;
        }

    }

    public class Goal
    {
        private Vector2[] goalLine;
        public Vector2[] GoalLine { get => goalLine; set => goalLine = value; }
        private GameSettings.Team owner;
        internal GameSettings.Team Owner { get => owner; set => owner = value; }

        public Goal(GameSettings.PlayerSide goalSide, GameSettings.Team _owner)
        {
            if (goalSide == GameSettings.PlayerSide.Left)
            {
                GoalLine = GoalSetup.LeftGoal();
            }
            if (goalSide == GameSettings.PlayerSide.Right)
            {
                GoalLine = GoalSetup.RightGoal();
            }
            owner = _owner;
        }
    }

    public class GameSettings
    {
        public enum PlayerSide
        {
            Right,
            Left
        }

        public enum Team
        {
            Player,
            Opponent
        }

        public enum EntityType
        {
            WIZARD, OPPONENT_WIZARD, SNAFFLE, BLUDGER
        }
        private static PlayerSide currentSidePlayer;
        public static PlayerSide CurrentSidePlayer { get => currentSidePlayer; set => currentSidePlayer = value; }
        private static PlayerSide currentSideOpponent;
        public static PlayerSide CurrentSideOpponent { get => currentSideOpponent; set => currentSideOpponent = value; }

        public static void SetupTeamId(int myTeamId)
        {
            if (myTeamId == 0)
            {
                CurrentSidePlayer = PlayerSide.Left;
                CurrentSideOpponent = PlayerSide.Right;
            }
            if (myTeamId == 1)
            {
                CurrentSidePlayer = PlayerSide.Right;
                CurrentSideOpponent = PlayerSide.Left;
            }
            game.PlayerGoal = new Goal(CurrentSidePlayer, Team.Player);
            game.OpponentGoal = new Goal(CurrentSideOpponent, Team.Opponent);
        }

        internal static void SetupEntities()
        {

            for (int i = 0; i < inputsEntities.Length; i++)
            {
                int entityId = int.Parse(inputsEntities[i][0]); // entity identifier
                string entityType = inputsEntities[i][1]; // "WIZARD", "OPPONENT_WIZARD" or "SNAFFLE" (or "BLUDGER" after first league)
                int x = int.Parse(inputsEntities[i][2]); // position
                int y = int.Parse(inputsEntities[i][3]); // position
                int vx = int.Parse(inputsEntities[i][4]); // velocity
                int vy = int.Parse(inputsEntities[i][5]); // velocity
                int state = int.Parse(inputsEntities[i][6]); // 1 if the wizard is holding a Snaffle, 0 otherwise
                CreateEntities(entityId, entityType, x, y, vx, vy, state);
            }
        }

        private static void CreateEntities(int _entityId, string _entityType, int _x, int _y, int _vx, int _vy, int _state)
        {
            if (_entityType == EntityType.WIZARD.ToString("G"))
            {
                game.Wizards[game.WizardsAlive] = new Wizard(_entityId, new Vector2(_x, _y), Team.Player, _state);
                game.WizardsAlive++;
            }

            if (_entityType == EntityType.OPPONENT_WIZARD.ToString("G"))
            {
                game.Wizards[game.WizardsAlive] = new Wizard(_entityId, new Vector2(_x, _y), Team.Opponent, _state);
                game.WizardsAlive++;
            }

            if (_entityType == EntityType.SNAFFLE.ToString("G"))
            {
                game.Snaffles[game.SnafflesAlive] = new Snaffle(_entityId, new Vector2(_x, _y));
                game.SnafflesAlive++;
            }

            if (_entityType == EntityType.BLUDGER.ToString("G"))
            {
                game.Bludgers[game.BludgersAlive] = new Bludgers(_entityId, new Vector2(_x, _y));
                game.BludgersAlive++;
            }


        }

        internal static void CreateGame()
        {
            GetNumberEntities();
            game = new Game();
        }

        private static void GetNumberEntities()
        {
            NumberBludgers = 0;
            NumberSnaffle = 0;
            NumberWizards = 0;
            for (int i = 0; i < inputsEntities.Length; i++)
            {
                string _entityType = inputsEntities[i][1]; // "WIZARD", "OPPONENT_WIZARD" or "SNAFFLE" (or "BLUDGER" after first league)
                if (_entityType == EntityType.WIZARD.ToString("G"))
                {
                    NumberWizards++;
                }

                if (_entityType == EntityType.OPPONENT_WIZARD.ToString("G"))
                {
                    NumberWizards++;
                }

                if (_entityType == EntityType.SNAFFLE.ToString("G"))
                {
                    NumberSnaffle++;
                }

                if (_entityType == EntityType.BLUDGER.ToString("G"))
                {
                    NumberBludgers++;
                }
            }
        }
    }

    public class Game
    {
        public Game()
        {
            Wizards = new Wizard[NumberWizards];
            Snaffles = new Snaffle[NumberSnaffle];
            Bludgers = new Bludgers[NumberBludgers];
        }

        private int wizardsAlive;
        public int WizardsAlive { get => wizardsAlive; set => wizardsAlive = value; }

        private int snafflesAlive;
        public int SnafflesAlive { get => snafflesAlive; set => snafflesAlive = value; }

        private int bludgersAlive;
        public int BludgersAlive { get => bludgersAlive; set => bludgersAlive = value; }

        Wizard[] wizards;
        internal Wizard[] Wizards { get => wizards; set => wizards = value; }

        Snaffle[] snaffles;
        internal Snaffle[] Snaffles { get => snaffles; set => snaffles = value; }

        Bludgers[] bludgers;
        internal Bludgers[] Bludgers { get => bludgers; set => bludgers = value; }

        private Goal opponentGoal;
        internal Goal OpponentGoal { get => opponentGoal; set => opponentGoal = value; }

        private Goal playerGoal;
        internal Goal PlayerGoal { get => playerGoal; set => playerGoal = value; }

    }

    public class Wizard
    {
        // Initialize
        private Vector2 position;
        public Vector2 Position { get => position; set => position = value; }
        private Vector2 nextPosition;
        public Vector2 NextPosition { get => nextPosition; set => nextPosition = value; }
        private Vector2 velocity;
        public Vector2 Velocity { get => velocity; set => velocity = value; }
        private GameSettings.Team team;
        internal GameSettings.Team Team { get => team; set => team = value; }
        private int id;
        public int Id { get => id; set => id = value; }



        // In Game
        public enum State
        {
            Free,
            Snaffle
        }
        State currentState;
        internal State CurrentState { get => currentState; set => currentState = value; }
        Snaffle holdedSnaffle;

        // Finish round
        public Command CommandPlayer;
        private string finalString;
        internal string FinalString { get => finalString; set => finalString = value; }


        public Wizard(int _id, Vector2 _position, GameSettings.Team _team, int _state)
        {
            id = _id;
            Position = _position;
            Team = _team;
            CurrentState = (State)_state;
        }


        public static Snaffle WizardSeekSnaffleClosest(Wizard wizard, Snaffle snaffleOtherWizard = null)
        {
            float distance = float.PositiveInfinity;
            Snaffle closestSnaffle = new Snaffle(0, Vector2.Zero);
            foreach (Snaffle snaffle in game.Snaffles)
            {
                if (snaffleOtherWizard != null && snaffle.Id == snaffleOtherWizard.Id)
                {
                    continue;
                }
                float snaffleToWizard = DistanceWizardSnaffle(wizard, snaffle);
                if (distance > snaffleToWizard)
                {
                    distance = snaffleToWizard;
                    closestSnaffle = snaffle;
                }
            }
            return closestSnaffle;
        }

        public static float DistanceWizardSnaffle(Wizard wizard,Snaffle snaffle)
        {
            return Vector2.Distance(wizard.position, snaffle.Position);
        }

        internal static Snaffle[] CheckNotSameSnaffle(Snaffle snaffle1, Snaffle snaffle2)
        {
            Snaffle[] snaffles = new Snaffle[2];
            snaffles[0] = snaffle1;
            snaffles[1] = snaffle2;
            if (snaffle1.Id == snaffle2.Id)
            {
                if (DistanceWizardSnaffle(game.Wizards[0], snaffle1) > DistanceWizardSnaffle(game.Wizards[1], snaffle2))
                {
                    snaffles[0] = WizardSeekSnaffleClosest(game.Wizards[0], snaffle1);
                }
                else
                {
                    snaffles[1] = WizardSeekSnaffleClosest(game.Wizards[1], snaffle2);
                }
            }
            return snaffles;
        }
    }
    public class Snaffle
    {
        private Vector2 position;
        public Vector2 Position { get => position; set => position = value; }
        private Vector2 velocity;
        public Vector2 Velocity { get => velocity; set => velocity = value; }
        private int id;
        public int Id { get => id; set => id = value; }

        public Snaffle(int _id, Vector2 _position)
        {
            id = _id;
            Position = _position;
        }
    }

    public class Bludgers
    {
        private Vector2 position;
        public Vector2 Position { get => position; set => position = value; }
        private Vector2 velocity;
        public Vector2 Velocity { get => velocity; set => velocity = value; }
        private int id;
        public int Id { get => id; set => id = value; }

        public Bludgers(int _id, Vector2 _position)
        {
            id = _id;
            Position = _position;
        }
    }

    public struct Command
    {
        public enum ActionType
        {
            MOVE,
            THROW
        }

        int posX, posY;
        string action;
        int thrust;

        public int PosX { get => posX; set => posX = value; }
        public int PosY { get => posY; set => posY = value; }
        public string Action { get => action; set => action = value; }
        public int Thrust { get => thrust; set => thrust = value; }

        public static string Builder(Command command)
        {
            string result = command.Action + " " + command.PosX.ToString() + " " + command.PosY.ToString() + " " + command.Thrust.ToString();
            return result;
        }
    }

    public static class Strategy
    {
        public static void BaseStrat()
        {
            if (game.Wizards[0].CurrentState == Wizard.State.Free)
            {
                // GO to snaffle
                game.Wizards[0].CommandPlayer.Action = Command.ActionType.MOVE.ToString("G");
                game.Wizards[0].CommandPlayer.PosX = (int)game.Snaffles[0].Position.X;
                game.Wizards[0].CommandPlayer.PosY = (int)game.Snaffles[0].Position.Y;
                game.Wizards[0].CommandPlayer.Thrust = 150;
                game.Wizards[0].FinalString = Command.Builder(game.Wizards[0].CommandPlayer);
            }
            else
            {
                game.Wizards[0].CommandPlayer.Action = Command.ActionType.THROW.ToString("G");
                game.Wizards[0].CommandPlayer.PosX = (int)game.OpponentGoal.GoalLine[1].X;
                game.Wizards[0].CommandPlayer.PosY = (int)game.OpponentGoal.GoalLine[1].Y;
                game.Wizards[0].CommandPlayer.Thrust = 500;
                game.Wizards[0].FinalString = Command.Builder(game.Wizards[0].CommandPlayer);
            }

            if (game.Wizards[1].CurrentState == Wizard.State.Free && game.Snaffles.Length > 1)
            {
                // GO to snaffle
                game.Wizards[1].CommandPlayer.Action = Command.ActionType.MOVE.ToString("G");
                game.Wizards[1].CommandPlayer.PosX = (int)game.Snaffles[1].Position.X;
                game.Wizards[1].CommandPlayer.PosY = (int)game.Snaffles[1].Position.Y;
                game.Wizards[1].CommandPlayer.Thrust = 150;
                game.Wizards[1].FinalString = Command.Builder(game.Wizards[1].CommandPlayer);
            }
            else if (game.Wizards[1].CurrentState == Wizard.State.Snaffle)
            {
                game.Wizards[1].CommandPlayer.Action = Command.ActionType.THROW.ToString("G");
                game.Wizards[1].CommandPlayer.PosX = (int)game.OpponentGoal.GoalLine[1].X;
                game.Wizards[1].CommandPlayer.PosY = (int)game.OpponentGoal.GoalLine[1].Y;
                game.Wizards[1].CommandPlayer.Thrust = 500;
                game.Wizards[1].FinalString = Command.Builder(game.Wizards[1].CommandPlayer);
            }
            else
            {
                game.Wizards[1].CommandPlayer.Action = Command.ActionType.MOVE.ToString("G");
                game.Wizards[1].CommandPlayer.PosX = 0;
                game.Wizards[1].CommandPlayer.PosY = 0;
                game.Wizards[1].CommandPlayer.Thrust = 0;
                game.Wizards[1].FinalString = Command.Builder(game.Wizards[1].CommandPlayer);
            }

        }

        public static void FirstAndFree()
        {
            Snaffle[] snaffles = new Snaffle[2];

            snaffles[0]= Wizard.WizardSeekSnaffleClosest(game.Wizards[0]);
            snaffles[1]= Wizard.WizardSeekSnaffleClosest(game.Wizards[1]);
            snaffles= Wizard.CheckNotSameSnaffle(snaffles[0], snaffles[1]);

            if (game.Wizards[0].CurrentState == Wizard.State.Free)
            {



                // GO to snaffle
                game.Wizards[0].CommandPlayer.Action = Command.ActionType.MOVE.ToString("G");
                game.Wizards[0].CommandPlayer.PosX = (int)snaffles[0].Position.X;
                game.Wizards[0].CommandPlayer.PosY = (int)snaffles[0].Position.Y;
                game.Wizards[0].CommandPlayer.Thrust = 150;
                game.Wizards[0].FinalString = Command.Builder(game.Wizards[0].CommandPlayer);
            }
            else
            {
                game.Wizards[0].CommandPlayer.Action = Command.ActionType.THROW.ToString("G");
                game.Wizards[0].CommandPlayer.PosX = (int)game.OpponentGoal.GoalLine[1].X;
                game.Wizards[0].CommandPlayer.PosY = (int)game.OpponentGoal.GoalLine[1].Y;
                game.Wizards[0].CommandPlayer.Thrust = 500;
                game.Wizards[0].FinalString = Command.Builder(game.Wizards[0].CommandPlayer);
            }

            if (game.Wizards[1].CurrentState == Wizard.State.Free && game.Snaffles.Length > 1)
            {
                // GO to snaffle
                game.Wizards[1].CommandPlayer.Action = Command.ActionType.MOVE.ToString("G");
                game.Wizards[1].CommandPlayer.PosX = (int)snaffles[1].Position.X;
                game.Wizards[1].CommandPlayer.PosY = (int)snaffles[1].Position.Y;
                game.Wizards[1].CommandPlayer.Thrust = 150;
                game.Wizards[1].FinalString = Command.Builder(game.Wizards[1].CommandPlayer);
            }
            else if (game.Wizards[1].CurrentState == Wizard.State.Snaffle)
            {
                game.Wizards[1].CommandPlayer.Action = Command.ActionType.THROW.ToString("G");
                game.Wizards[1].CommandPlayer.PosX = (int)game.OpponentGoal.GoalLine[1].X;
                game.Wizards[1].CommandPlayer.PosY = (int)game.OpponentGoal.GoalLine[1].Y;
                game.Wizards[1].CommandPlayer.Thrust = 500;
                game.Wizards[1].FinalString = Command.Builder(game.Wizards[1].CommandPlayer);
            }
            else
            {
                game.Wizards[1].CommandPlayer.Action = Command.ActionType.MOVE.ToString("G");
                game.Wizards[1].CommandPlayer.PosX = 0;
                game.Wizards[1].CommandPlayer.PosY = 0;
                game.Wizards[1].CommandPlayer.Thrust = 0;
                game.Wizards[1].FinalString = Command.Builder(game.Wizards[1].CommandPlayer);
            }
        }
    }
}

