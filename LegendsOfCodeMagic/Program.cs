using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Codeingame
{
    static void Main(string[] args)
    {
        Game game = new Game();
        PlayerManager playerManager = new PlayerManager();
        // game loop
        while (true)
        {

            for (int i = 0; i < 2; i++)
            {
                // PlayerValue
                string[] playerValue = Console.ReadLine().Split(' ');
                playerManager.AddPlayer(playerValue, i);
            }
            string[] OpponentInfo = Console.ReadLine().Split(' ');
            int opponentHand = int.Parse(OpponentInfo[0]);
            int opponentActions = int.Parse(OpponentInfo[1]);
            for (int i = 0; i < opponentActions; i++)
            {
                string cardNumberAndAction = Console.ReadLine();
            }
            // un entier cardCount, le nombre total de cartes sur le plateau de jeu et dans la main du joueur actif.
            Board activeBoard = new Board();
            int cardCount = int.Parse(Console.ReadLine());
            for (int i = 0; i < cardCount; i++)
            {
                string[] cardActive = Console.ReadLine().Split(' ');
                activeBoard.AddCardOnBoard(cardActive);
            }
            game.Board = activeBoard;
            game.Clear();
            // Write an action using Console.WriteLine()
            // To debug: Console.Error.WriteLine("Debug messages...");
            if (game.Round >= 30)
            {
                game.CurrentPhase = Game.GamePhase.Versus;
                game.SetPlayerNumber();
            }
            if (game.CurrentPhase == Game.GamePhase.Draft)
            {
                DraftTactics.PickHightValueCard(game);
                game.CompileLastWord();
                Console.WriteLine(game.LastWord);
            }
            else
            {
                //VersusTactics.PlayIfPossible(game);
                VersusTactics.Wood2(game);
                game.CompileLastWord();
                Console.WriteLine(game.LastWord);
            }
            game.Round++;
        }
    }

    private class Board
    {
        private List<Card> totalCardOnBoard = new List<Card>();
        public List<Card> TotalCardOnBoard { get => totalCardOnBoard; set => totalCardOnBoard = value; }

        private List<Card> playerBoard = new List<Card>();
        public List<Card> PlayerBoard { get => playerBoard; set => playerBoard = value; }

        private List<Card> opponentBoard = new List<Card>();
        public List<Card> OpponentBoard { get => opponentBoard; set => opponentBoard = value; }

        private List<Card> playerHand = new List<Card>();

        public Board()
        {
            TotalCardOnBoard = new List<Card>();
            PlayerBoard = new List<Card>();
            OpponentBoard = new List<Card>();
            Debug.Log("new board");
        }

        public List<Card> PlayerHand { get => playerHand; set => playerHand = value; }

        internal void AddCardOnBoard(string[] cardActive)
        {
            int cardNumber = int.Parse(cardActive[0]);
            int instanceId = int.Parse(cardActive[1]);
            int location = int.Parse(cardActive[2]);
            int cardType = int.Parse(cardActive[3]);
            int cost = int.Parse(cardActive[4]);
            int attack = int.Parse(cardActive[5]);
            int defense = int.Parse(cardActive[6]);
            string abilities = cardActive[7];
            int myHealthChange = int.Parse(cardActive[8]);
            int opponentHealthChange = int.Parse(cardActive[9]);
            int cardDraw = int.Parse(cardActive[10]);
            TotalCardOnBoard.Add(new Card(cardNumber, instanceId, location, cardType, cost, attack, defense, abilities, myHealthChange, opponentHealthChange, cardDraw));
            SortCard();
        }

        private void SortCard()
        {
            Card newCard = TotalCardOnBoard[TotalCardOnBoard.Count - 1];
            switch (newCard.Location)
            {
                case 0:
                    playerHand.Add(newCard);
                    break;

                case 1:
                    playerBoard.Add(newCard);
                    break;

                case -1:
                    OpponentBoard.Add(newCard);
                    break;
            }
        }
    }

    private static class DraftTactics
    {
        //internal static string PickLowValueCard()
        //{
        //    List<Card> sortedCard = Game.Instance.Board.TotalCardOnBoard.OrderBy(x => x.Attack).ToList();
        //    int cardHightValue = Game.Instance.Board.TotalCardOnBoard.IndexOf(sortedCard[0]);
        //    game"PICK " + cardHightValue;
        //}

        internal static void PickHightValueCard(Game game)
        {
            List<Card> sortedCard = Game.Instance.Board.TotalCardOnBoard.OrderByDescending(x => x.Attack).ToList();
            int cardHightValue = Game.Instance.Board.TotalCardOnBoard.IndexOf(sortedCard[0]);
            game.PossibleAction.Add(Game.PICK + cardHightValue);
        }
    }

    private static class VersusTactics
    {
        static List<Card> summonCards = new List<Card>();
        private static List<Card> SummonCards { get => summonCards; set => summonCards = value; }

        internal static void ResetVersusTactics()
        {
            SummonCards.Clear();
        }

        internal static void PlayIfPossible(Game game)
        {
            int manaAvailable = PlayerManager.Instance.GetMainPlayer().PlayerMana;
            Card selectedCard = null;
            foreach (Card card in Game.Instance.Board.PlayerHand)
            {
                if (card.Cost <= manaAvailable)
                {
                    selectedCard = card;
                    break;
                }
            }
            if (selectedCard != null && Game.Instance.Board.PlayerBoard.Count <= 6)
            {
                game.PossibleAction.Add(Game.SUMMON + selectedCard.InstanceId);
            }
            if (Game.Instance.Board.PlayerBoard.Count > 0)
            {
                List<Card> sortedCard = Game.Instance.Board.PlayerBoard.OrderBy(x => x.Attack).ToList();
                game.PossibleAction.Add(Game.ATTACK + sortedCard[0].InstanceId + " -1");
            }
        }


        internal static void Wood2(Game game)
        {
            int manaAvailable = PlayerManager.Instance.GetMainPlayer().PlayerMana;
            SelectNewCardInHand(manaAvailable);
            foreach (Card cards in SummonCards)
            {
                if (game.Board.PlayerBoard.Count <= 6)
                {
                    game.PossibleAction.Add(Game.SUMMON + cards.InstanceId);
                }
            }
            AttackStrategy(game);
        }

        private static void AttackStrategy(Game game)
        {
            List<Card> blockers = new List<Card>();
            string target = "-1"; // Opponent
            foreach (Card opponentCard in game.Board.OpponentBoard)
            {
                if (opponentCard.IsGuard)
                {
                    blockers.Add(opponentCard);
                }
            }
            if (blockers.Count > 0 && Game.Instance.Board.PlayerBoard.Count > 0)
            {
                KillBlockers(blockers, game);
            }
            if (Game.Instance.Board.PlayerBoard.Count > 0)
            {
                foreach (Card mycard in Game.Instance.Board.PlayerBoard)
                {
                    game.PossibleAction.Add(Game.ATTACK + mycard.InstanceId + " " + target);
                }
            }

        }

        private static void KillBlockers(List<Card> blockers, Game game)
        {
            game.PossibleAction.Add(Game.ATTACK + Game.Instance.Board.PlayerBoard[0].InstanceId + " " + blockers[0].InstanceId.ToString());

            if ((Game.Instance.Board.PlayerBoard[0].Attack - blockers[0].Defense) > 0)
            {
                // I kill you
                blockers.Remove(blockers[0]);
                Game.Instance.Board.PlayerBoard.Remove(Game.Instance.Board.PlayerBoard[0]);
                if (blockers.Count > 0 && Game.Instance.Board.PlayerBoard.Count > 0)
                {
                    KillBlockers(blockers, game);
                }
            }
            else
            {
                Game.Instance.Board.PlayerBoard.Remove(Game.Instance.Board.PlayerBoard[0]);
                if (blockers.Count > 0 && Game.Instance.Board.PlayerBoard.Count > 0)
                {
                    KillBlockers(blockers, game);
                }
            }
        }

        private static void SelectNewCardInHand(int _currentMana)
        {
            List<Card> tempList = new List<Card>(Game.Instance.Board.PlayerHand);
            foreach (Card card in tempList)
            {
                if (card.Cost - _currentMana >= 0)
                {
                    SummonCards.Add(card);
                    Game.Instance.Board.PlayerHand.Remove(card);

                    _currentMana -= card.Cost;
                    if (_currentMana > 0)
                    {
                        SelectNewCardInHand(_currentMana);
                    }
                }
            }
        }
    }

    private class PlayerManager
    {
        List<Player> playerList = new List<Player>();
        public List<Player> PlayerList { get => playerList; set => playerList = value; }

        private static PlayerManager instance;
        public static PlayerManager Instance
        {
            get
            {
                return instance;
            }
        }


        private void Awake()
        {

            if (instance == null)
            {
                instance = this;
            }
            else
            {
                return;
            }
        }

        internal void AddPlayer(string[] _playerValue, int _index)
        {
            int playerHealth = int.Parse(_playerValue[0]);
            int playerMana = int.Parse(_playerValue[1]);
            int playerDeck = int.Parse(_playerValue[2]);
            int playerRune = int.Parse(_playerValue[3]);
            int playerDraw = int.Parse(_playerValue[4]);

            if (PlayerList.Count < 2)
            {
                PlayerList.Add(new Player(_index, playerHealth, playerMana, playerDeck, playerRune, playerDraw));
            }
            else
            {
                PlayerList[_index].PlayerHealth = playerHealth;
                PlayerList[_index].PlayerMana = playerMana;
                PlayerList[_index].PlayerDeck = playerDeck;
                PlayerList[_index].PlayerRune = playerRune;
                PlayerList[_index].PlayerDraw = playerDraw;
            }

        }

        internal Player GetMainPlayer()
        {
            return PlayerList[Game.Instance.PlayerNumber];
        }

        public PlayerManager()
        {
            Awake();
        }
    }

    private class Player
    {
        int playerID, playerHealth, playerMana, playerDeck, playerRune, playerDraw;

        public Player(int playerID, int playerHealth, int playerMana, int playerDeck, int playerRune, int playerDraw)
        {
            this.PlayerID = playerID;
            this.playerHealth = playerHealth;
            this.playerMana = playerMana;
            this.playerDeck = playerDeck;
            this.playerRune = playerRune;
            this.playerDraw = playerDraw;
        }

        public int PlayerID { get => playerID; set => playerID = value; }
        public int PlayerHealth { get => playerHealth; set => playerHealth = value; }
        public int PlayerMana { get => playerMana; set => playerMana = value; }
        public int PlayerDeck { get => playerDeck; set => playerDeck = value; }
        public int PlayerRune { get => playerRune; set => playerRune = value; }
        public int PlayerDraw { get => playerDraw; set => playerDraw = value; }
    }

    private class Deck
    {
        private List<Card> deckList = new List<Card>();
        public Deck()
        {

        }

        internal void AddCard(Card _card)
        {
            deckList.Add(_card);
        }

        internal void RemoveCard(Card _card)
        {
            deckList.Remove(_card);
        }
    }

    private class Card
    {
        private int cardNumber, instanceId, location, cardType, cost, attack, defense, myhealthChange, opponentHealthChange, cardDraw;
        private string abilities;

        private bool isGuard, isCharge, isBreakthrough;

        internal const char ABILITIES_BREAKTHROUGH = 'B';
        internal const char ABILITIES_CHARGE = 'C';
        internal const char ABILITIES_GUARD = 'G';

        public Card(int _cardNumber, int _instanceId, int _location, int _cardType, int _cost, int _attack, int _defense, string _abilities, int _myHealthChange, int _opponentHealthChange, int _cardDraw)
        {
            this.CardNumber = _cardNumber;
            this.InstanceId = _instanceId;
            this.Location = _location;
            this.CardType = _cardType;
            this.Cost = _cost;
            this.Attack = _attack;
            this.Defense = _defense;
            this.Abilities = _abilities;
            this.MyhealthChange = _myHealthChange;
            this.OpponentHealthChange = _opponentHealthChange;
            this.CardDraw = _cardDraw;
        }

        public int CardNumber { get => cardNumber; set => cardNumber = value; }
        public int InstanceId { get => instanceId; set => instanceId = value; }
        public int Location { get => location; set => location = value; }
        public int CardType { get => cardType; set => cardType = value; }
        public int Cost { get => cost; set => cost = value; }
        public int Attack { get => attack; set => attack = value; }
        public int Defense { get => defense; set => defense = value; }
        public string Abilities
        {
            get => abilities; set
            {
                abilities = value;
                ParseAbilities();
            }
        }
        public int MyhealthChange { get => myhealthChange; set => myhealthChange = value; }
        public int OpponentHealthChange { get => opponentHealthChange; set => opponentHealthChange = value; }
        public int CardDraw { get => cardDraw; set => cardDraw = value; }

        // Personnal stuff
        public bool IsGuard { get => isGuard; set => isGuard = value; }
        public bool IsCharge { get => isCharge; set => isCharge = value; }
        public bool IsBreakthrough { get => isBreakthrough; set => isBreakthrough = value; }

        public int SortByAttackAscending(int attack1, int attack2)
        {
            return attack1.CompareTo(attack2);
        }

        private void ParseAbilities()
        {
            foreach (char abi in abilities)
            {
                switch (abi)
                {
                    case ABILITIES_BREAKTHROUGH:
                        IsBreakthrough = true;
                        break;
                    case ABILITIES_CHARGE:
                        IsCharge = true;
                        break;
                    case ABILITIES_GUARD:
                        IsGuard = true;
                        break;
                }
            }
        }
    }

    private class Hand
    {
        private List<Card> handCard = new List<Card>();
    }

    private class Game
    {
        public static string PICK = "PICK ";
        public static string SUMMON = "SUMMON ";
        public static string PASS = "PASS ";
        public static string ATTACK = "ATTACK ";
        public enum GamePhase
        {
            Draft,
            Versus,
        }
        private GamePhase currentPhase;
        public GamePhase CurrentPhase { get => currentPhase; set => currentPhase = value; }

        private string lastWord;
        public string LastWord { get => lastWord; set => lastWord = value; }

        private List<string> possibleAction = new List<string>();
        public List<string> PossibleAction { get => possibleAction; set => possibleAction = value; }

        private int round;
        public int Round { get => round; set => round = value; }

        private int playerNumber;
        public int PlayerNumber { get => playerNumber; set => playerNumber = value; }
        public Board Board { get; internal set; }

        private static Game instance;
        public static Game Instance
        {
            get
            {
                return instance;
            }
        }



        private void Awake()
        {

            if (instance == null)
            {
                instance = this;
            }
            else
            {
                return;
            }
        }


        public Game()
        {
            CurrentPhase = GamePhase.Draft;
            Round = 0;
            Awake();
        }

        internal void SetPlayerNumber()
        {
            if (Game.Instance.Board.PlayerHand.Count == 4)
            {
                playerNumber = 0;
            }
            else
            {
                playerNumber = 1;
            }
        }

        internal void CompileLastWord()
        {
            foreach (string word in possibleAction)
            {
                lastWord += word;
                if (GamePhase.Versus == currentPhase)
                {
                    lastWord += ";";
                }
            }
            if (possibleAction.Count == 0)
            {
                lastWord = Game.PASS;
            }
        }

        internal void Clear()
        {
            PossibleAction.Clear();
            lastWord = "";

            VersusTactics.ResetVersusTactics();
        }
    }

    private static class Debug
    {
        public static void Log(string log)
        {
            Console.Error.WriteLine(log);
        }

        public static void Card(Card card)
        {
            Console.Error.WriteLine(
                 "this.CardNumber " + card.CardNumber + "\n" +
            "this.InstanceId " + card.InstanceId + "\n" +
            "this.Location " + card.Location + "\n" +
            "this.CardType " + card.CardType + "\n" +
            "this.Cost " + card.Cost + "\n" +
            "this.Attack " + card.Attack + "\n" +
            "this.Defense " + card.Defense + "\n" +
            "this.Abilities " + card.Abilities + "\n" +
            "this.MyhealthChange " + card.MyhealthChange + "\n" +
            "this.OpponentHealthChange " + card.OpponentHealthChange + "\n" +
            "this.CardDraw " + card.CardDraw);
        }

    }
}