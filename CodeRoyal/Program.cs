using System;
using System.Collections.Generic;
using System.Linq;
internal enum OwnerType {
    None = -1,
    Friendly = 0,
    Enemy = 1
}

internal enum UnitType {
    QUEEN = -1,
    KNIGHT = 0,
    ARCHER = 1,
    GIANT = 2
}

internal enum BuildType {
    NONE = -1,
    MINE = 0,
    TOWER = 1,
    BARRACKS = 2
}

internal class Site {
    public BuildType Type { get; set; }
    public int MaxMineSize { get; set; }
    public int Id { get; set; }
    public int X { get; set; }
    public int Y { get; set; }
    public int Radius { get; set; }
    public OwnerType Owner { get; set; }
    public int GoldRemaining { get; set; }
    public int Param1 { get; set; }
    public int Param2 { get; set; }
    public void Log() {
        Debug.Log($"Site - Id: {Id}, X: {X}, Y: {Y}, Radius: {Radius}, Gold: {GoldRemaining}, MaxMineSize: {MaxMineSize}, StructureType: {Type.ToString()}, Owner: {Owner.ToString()}, Param1: {Param1}, Param2: {Param2}");
    }
}

internal class Unit {
    public int X { get; set; }
    public int Y { get; set; }
    public OwnerType Owner { get; set; }
    public UnitType Type { get; set; }
    public int Health { get; set; }
    public void Log() {
        Debug.Log($"Unit - X: {X}, Y: {Y}, Owner: {Owner.ToString()}, Type: {Type.ToString()}, Health: {Health}");
    }
}

internal class Game {
    public List<Site> Sites { get; set; } = new List<Site>();
    public List<Unit> Units { get; set; } = new List<Unit>();
    public int Gold { get; set; }
    public int TouchedSite { get; set; }

    public void InitSites() {
        int numSites = int.Parse(Console.ReadLine());
        for (int i = 0; i < numSites; i++) {
            string[] inputs = Console.ReadLine().Split(' ');
            Sites.Add(new Site {
                Id = int.Parse(inputs[0]),
                X = int.Parse(inputs[1]),
                Y = int.Parse(inputs[2]),
                Radius = int.Parse(inputs[3])
            });
        }
    }

    public void UpdateGameState() {
        string[] inputs = Console.ReadLine().Split(' ');

        Gold = int.Parse(inputs[0]);
        TouchedSite = int.Parse(inputs[1]);
        Units.Clear();
        for (int i = 0; i < Sites.Count; i++) {
            string inputBase = Console.ReadLine();
            string[] inputsBuilding = inputBase.Split(' ');
            Site currentSite = Sites[i];
            currentSite.GoldRemaining = int.Parse(inputsBuilding[1]); // -1 if unknown
            currentSite.MaxMineSize = int.Parse(inputsBuilding[2]); // -1 if unknown
            currentSite.Type = (BuildType)int.Parse(inputsBuilding[3]);
            currentSite.Owner = (OwnerType)int.Parse(inputsBuilding[4]);
            currentSite.Param1 = int.Parse(inputsBuilding[5]);
            currentSite.Param2 = int.Parse(inputsBuilding[6]);
        }

        int numUnits = int.Parse(Console.ReadLine());
        for (int i = 0; i < numUnits; i++) {
            string[] inputsUnits = Console.ReadLine().Split(' ');
            Units.Add(new Unit {
                X = int.Parse(inputsUnits[0]),
                Y = int.Parse(inputsUnits[1]),
                Owner = (OwnerType)int.Parse(inputsUnits[2]),
                Type = (UnitType)int.Parse(inputsUnits[3]),
                Health = int.Parse(inputsUnits[4])
            });
        }
    }

    public string DecideMove() {
        // Basic strategy: Move the queen around randomly
        Site targetSite = Sites[new Random().Next(0, Sites.Count)];
        return $"MOVE {targetSite.X} {targetSite.Y}";
    }

    public string DecideWait() {
        return "WAIT";
    }

    public string DecideBuild() {
        // Basic strategy: Try to build mines
        Site targetSite = Sites[new Random().Next(0, Sites.Count)];
        return $"BUILD {targetSite.Id} MINE";
    }

    public string DecideTrain() {
        return "TRAIN";
    }
}

internal class Player {
    private static void Main(string[] args) {
        Game game = new Game();
        game.InitSites();
        while (true) {
            game.UpdateGameState();

            // Identify the Queen
            Unit queen = game.Units.FirstOrDefault(unit => unit.Type == UnitType.QUEEN && unit.Owner == OwnerType.Friendly);

            if (queen == null) {
                Debug.Log("No Queen");
                Console.WriteLine("WAIT");
            }
            else {
                Site closestSite = null;
                double minDistance = double.MaxValue;
                foreach (Site site in game.Sites) {
                    if (site.Type != BuildType.NONE) {
                        continue;
                    }
                    double dx = queen.X - site.X;
                    double dy = queen.Y - site.Y;
                    double distance = Math.Sqrt(dx * dx + dy * dy);
                    if (distance < minDistance) {
                        minDistance = distance;
                        closestSite = site;
                    }
                }
                if (closestSite != null && game.TouchedSite == closestSite.Id) {
                    Console.WriteLine(game.DecideBuild());
                }
                else if (closestSite != null) {
                    Console.WriteLine("MOVE " + closestSite.X + " " + closestSite.Y);
                }
                else {
                    Console.WriteLine(game.DecideWait());
                }
            }
            Console.WriteLine(game.DecideTrain());
        }
    }
}
internal class Debug {
    public static void Log(string message) {
        Console.Error.WriteLine(message);
    }
}