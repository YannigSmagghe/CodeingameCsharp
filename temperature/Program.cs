﻿using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Solution
{
    static void Main(string[] args)
    {
        int n = int.Parse(Console.ReadLine()); // the number of temperatures to analyse
        string[] inputs = Console.ReadLine().Split(' ');

        List<int> listTemp = new List<int>();
        List<int> listTempPoisitive = new List<int>();
        if (inputs != null)
        {
            for (int i = 0; i < n; i++)
            {
                int t = int.Parse(inputs[i]); // a temperature expressed as an integer ranging from -273 to 5526
                int tPositive = t;
                if (t < 0)
                {
                    tPositive = t - (t * 2);
                    listTempPoisitive.Add(tPositive);
                }
                else
                {
                    listTempPoisitive.Add(tPositive);
                };


                listTemp.Add(t);
            }
        }
        
        Console.Error.WriteLine(String.Join("; ", listTemp));
        bool isEmpty = !listTemp.Any();
        if (isEmpty)
        {
            Console.WriteLine("0");
        }else
        {
            listTempPoisitive.Sort();
            if (listTemp.Contains(listTempPoisitive[0]))
            {
                Console.WriteLine(listTempPoisitive[0]);
            }
            else
            {
                int result = listTempPoisitive[0];
                result = result - (result * 2);
                Console.WriteLine(result);
            }
        }
    }
}