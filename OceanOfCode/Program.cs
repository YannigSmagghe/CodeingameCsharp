﻿using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Program
{
    static void Main(string[] args)
    {
        Player player = new Player();
        string[] inputs;
        inputs = Console.ReadLine().Split(' ');
        int width = int.Parse(inputs[0]);
        int height = int.Parse(inputs[1]);
        int myId = int.Parse(inputs[2]);
        for (int i = 0; i < height; i++)
        {
            string line = Console.ReadLine();
            for (int j = 0; j < line.Length; j++)
            {
                Map.MapData.Add(new Cell(new Vector2(j,i), Map.CheckCellLineFree(line[j])));
            }

        }

        // Write an action using Console.WriteLine()
        // To debug: Console.Error.WriteLine("Debug messages...");

        //Start point
        Console.WriteLine("8 8");
        player.CurrentPosition = new Vector2(8, 8);

        //Debug.MapData();
        // game loop
        while (true)
        {
            inputs = Console.ReadLine().Split(' ');
            int x = int.Parse(inputs[0]);
            int y = int.Parse(inputs[1]);
            int myLife = int.Parse(inputs[2]);
            int oppLife = int.Parse(inputs[3]);
            int torpedoCooldown = int.Parse(inputs[4]);
            int sonarCooldown = int.Parse(inputs[5]);
            int silenceCooldown = int.Parse(inputs[6]);
            int mineCooldown = int.Parse(inputs[7]);
            string sonarResult = Console.ReadLine();
            string opponentOrders = Console.ReadLine();

            // Write an action using Console.WriteLine()
            // To debug: Console.Error.WriteLine("Debug messages...");
            Vector2 direction = new Vector2(-1, 0);
            Vector2 nextPosition = direction + player.CurrentPosition;
            
            if (!Map.CellAvailable(nextPosition))
            {
                FindNewDirection(nextPosition);
            }
            else
            {

            }
            Console.WriteLine("MOVE " + GetCardinalDirection(player.CurrentPosition - nextPosition) + " TORPEDO");
            player.CurrentPosition = nextPosition;
        }
    }

    private static void FindNewDirection(Vector2 nextPosition)
    {
        throw new NotImplementedException();
    }

    static string GetCardinalDirection(Vector2 desiredDirection)
    {
        if (desiredDirection == new Vector2(1, 0))
        {
            return "E";
        }
        else if (desiredDirection == new Vector2(0, -1))
        {
            return "N";
        }
        else if (desiredDirection == new Vector2(-1, 0))
        {
            return "W";
        }
        else if (desiredDirection == new Vector2(0, 1))
        {
            return "S";
        }
        else
        {
            return "";
        }
    }
}

internal class Player
{
    private Vector2 currentPosition;
    public Vector2 CurrentPosition { get => currentPosition; set => currentPosition = value; }
}

internal class Map
{
    private static List<Cell> mapData = new List<Cell>();
    internal static List<Cell> MapData { get => mapData; set => mapData = value; }

    internal static bool CellAvailable(Vector2 nextPosition)
    {
        bool result = false;
        foreach (Cell cell in MapData)
        {
            if (cell.Coord == nextPosition)
            {
                if (cell.Free)
                {
                    Debug.Log("cellAvalaible " + cell.Coord + " is free " + cell.Free);
                    result = true;
                }
                else
                {
                    Debug.Log("cellAvalaible " + cell.Coord + " is not free " + cell.Free);
                }
            }
            else
            {
                continue;
            }
        }
        return result;

    }

    internal static bool CheckCellLineFree(char line)
    {
        if (line == 'x')
        {
            return false;
        }
        else if (line == '.')
        {
            return true;
        }
        else
        {
            Debug.Log("line non reconnue");
            return false;
        }
    }
}

internal class Cell
{
    private Vector2 coord;
    public Vector2 Coord { get => coord; set => coord = value; }
    private bool free;
    public bool Free { get => free; set => free = value; }

    public Cell(Vector2 coord, bool free)
    {
        this.Coord = coord;
        this.Free = free;
    }

}

public static class Debug
{
    public static void Log(string log)
    {
        Console.Error.WriteLine(log);
    }

    internal static void MapData()
    {
        foreach (Cell cell in Map.MapData)
        {
            Debug.Log("coord" + cell.Coord);
            Debug.Log("free" + cell.Free);
        }
    }
}