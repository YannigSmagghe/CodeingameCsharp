﻿using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Solution
{
    public const string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    static void Main(string[] args)
    {

        int L = int.Parse(Console.ReadLine());
        int H = int.Parse(Console.ReadLine());
        string text = Console.ReadLine().ToUpper();
        for (int i = 0; i < H; i++)
        {
            string ROW = Console.ReadLine();
            string result = string.Empty;
            for (int j = 0; j < text.Length; j++)
            {
                int indexLetter = alphabet.IndexOf(text[j]);
                //Console.Error.WriteLine(indexLetter);
                result += ROW.Substring((indexLetter == -1 ? 26 : indexLetter) * L, L);
                //Console.Error.WriteLine(result);
            }
            Console.WriteLine(result);
        }

        // Write an action using Console.WriteLine()
        // To debug: Console.Error.WriteLine("Debug messages...");

        
    }
}