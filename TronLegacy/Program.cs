﻿using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Security.Cryptography.X509Certificates;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Player
{
    static Dictionary<string, Coord> dicDir = new Dictionary<string, Coord> {
        { "UP", new Coord(0,-1) },
        { "LEFT", new Coord(-1, 0) },
        { "DOWN", new Coord(0, 1) },
        { "RIGHT", new Coord(1, 0) },
    };

    private const int MAX_HEIGHT = 20; // hauteur
    private const int MAX_WIDTH = 30; // largeur
    private const string NODE_START = "START"; // largeur
    private const string NODE_END = "END"; // largeur
    private const string NODE_VISITED = "VISITED"; // largeur
    private const string NODE_UNVISITED = "UNVISITED"; // largeur


    static List<Node> mapGrid = new List<Node>();
    static List<Node> unvisitedNodeList = new List<Node>();
    static List<Node> aStarOpenList = new List<Node>();
    static List<Node> aStarCloseList = new List<Node>();

    static List<Link> links = new List<Link>();
    static List<string> removeNode = new List<string>();

    static Node currentNode = null;
    static Node startNode = null;
    static Node endNode = null;
    static string mydirection = null;
    static string myDirection = "LEFT";

    public static void CreateMap()
    {
        for (int x = 0; x < MAX_WIDTH; x++)
        {
            for (int y = 0; y < MAX_HEIGHT; y++)
            {
                mapGrid.Add(new Node("X" + x + "Y" + y, (new Coord(x, y)), new List<Link>(), NODE_UNVISITED, new List<Node>()));

            }

        }
    }


    static void Main(string[] args)
    {
        string[] inputs;
        /** Create map with every node in what weird wat did u create this project ? let's recreate **/
        CreateMap();
        links.Clear();


        //Int32 unixTimestamp1 = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
        // game loop
        while (true)
        {

            links.Clear();
            removeNode.Clear();

            currentNode = null;
            startNode = null;
            endNode = null;
            mydirection = null;



            inputs = Console.ReadLine().Split(' ');
            int N = int.Parse(inputs[0]); // total number of players (2 to 4).
            //int N = 2; // total number of players (2 to 4).
            int P = int.Parse(inputs[1]); // your player number (0 to 3).
            //int P = 0; // your player number (0 to 3).

            for (int i = 0; i < N; i++)
            {
                inputs = Console.ReadLine().Split(' ');
                //inputs = "0 10 14 13".Split(' ');

                int X0 = int.Parse(inputs[0]); // starting X coordinate of lightcycle (or -1)
                int Y0 = int.Parse(inputs[1]); // starting Y coordinate of lightcycle (or -1)
                int X1 = int.Parse(inputs[2]); // starting X coordinate of lightcycle (can be the same as X0 if you play before this player)
                int Y1 = int.Parse(inputs[3]); // starting Y coordinate of lightcycle (can be the same as Y0 if you play before this player)
                //Console.Error.WriteLine(X1);
                //Console.Error.WriteLine(Y1);
                if (i == P)
                {

                    SetStartNode(X1, Y1);
                    //SetStartNode(0, 1);
                    //Console.Error.WriteLine("current + start");
                    //Console.Error.WriteLine(startNode);
                    //Console.Error.WriteLine(currentNode != null);

                }
                else
                {

                    SetEndNode(X1, Y1);
                    //SetEndNode(14, 13);
                    //Console.Error.WriteLine("end");
                    //Console.Error.WriteLine(endNode);
                }
                PrepareToRemove(X1, Y1);

            }



            CreateLinkToNode();
            Console.Error.WriteLine(links.Count);
            unvisitedNodeList.Clear();
            SetUnvisitedList();
            /** Dijkstra **/
            //Dijkstra.FindShortestPath();

            /** A* */
            initAStar();
            FindShortestPathAstar();


            string[] neighboursPath = endNode.ShortestPath.Split(' ');
            //Console.Error.WriteLine(neighboursPath[2]);
            foreach (var path in neighboursPath)
            {
                Console.Error.WriteLine("path");
                Console.Error.WriteLine(path);
            }
            if (neighboursPath.Length <= 3)
            {
                Console.Error.WriteLine("short path");
                //Node targetNeighbour = mapGrid.Find(x => x.Name == neighboursPath[1]);
                //Console.Error.WriteLine(targetNeighbour);
                //Console.Error.WriteLine(startNode);
                //Console.Error.WriteLine(endNode);
                Node newTarget = startNode.Neighbours.Find(x => x.Name != endNode.Name);
                //Console.Error.WriteLine(startNode.Name);
                //Console.Error.WriteLine(currentNode.Name);
                //Console.Error.WriteLine(newTarget.ToString());
                int nextCoordX = newTarget.Position.x - startNode.Position.x;
                int nextCoordY = newTarget.Position.y - startNode.Position.y;
                Coord nextCoord = new Coord(nextCoordX, nextCoordY);
                myDirection = dicDir.First(x => x.Value.x == nextCoord.x && x.Value.y == nextCoord.y).Key;
                //Dijkstra.FindShortestPath();
            }
            else
            {
                Console.Error.WriteLine("long path");
                Node targetNeighbour = mapGrid.Find(x => x.Name == neighboursPath[2]);
                int nextCoordX = targetNeighbour.Position.x - startNode.Position.x;
                int nextCoordY = targetNeighbour.Position.y - startNode.Position.y;
                Coord nextCoord = new Coord(nextCoordX, nextCoordY);
                myDirection = dicDir.First(x => x.Value.x == nextCoord.x && x.Value.y == nextCoord.y).Key;
            }

            // A*
            //if (neighboursPath[2] != null)
            //{
            //    Node targetNeighbour = mapGrid.Find(x => x.Name == neighboursPath[2]);
            //    int nextCoordX = targetNeighbour.Position.x - startNode.Position.x;
            //    int nextCoordY = targetNeighbour.Position.y - startNode.Position.y;
            //    Coord nextCoord = new Coord(nextCoordX, nextCoordY);
            //    myDirection = dicDir.First(x => x.Value.x == nextCoord.x && x.Value.y == nextCoord.y).Key;
            //}
            //else
            //{
            //    myDirection = "nul";
            //}



            //Console.Error.WriteLine("test");
            //Console.Error.WriteLine(endNode.Name);
            //Console.Error.WriteLine(endNode.ShortestPath);
            //Console.Error.WriteLine("ma node");
            //Console.Error.WriteLine(endNode.ShortestPath);
            /** result **/
            //Console.WriteLine("RIGHT");
            Console.WriteLine(myDirection);


            //Console.Error.WriteLine("current");
            //Console.Error.WriteLine(currentNode);
            //Console.Error.WriteLine("end");
            //Console.Error.WriteLine(endNode);


            foreach (var nodeName in removeNode)
            {
                Node removeThisNode = mapGrid.Find(x => x.Name == nodeName);
                if (removeThisNode != null)
                {
                    RemoveColoriseNode(removeThisNode.Position.x, removeThisNode.Position.y);
                }

            }
            unvisitedNodeList.Clear();
            //Console.Error.WriteLine(myDirection);
            //Console.ReadKey();
            //Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
            //var delay =  unixTimestamp-unixTimestamp1;
            //Console.Error.WriteLine(delay);
        }

    }

    #region Dijkstra
    private static void SetStartNode(int x0, int y0)
    {
        Node node = mapGrid.Find(x => x.Position.x == x0 && x.Position.y == y0);
        startNode = node;
        startNode.Distance = 0;
        startNode.Visited = NODE_START;
        currentNode = node;
        startNode = node;

    }

    private static void SetEndNode(int x1, int y1)
    {
        Node node = mapGrid.Find(x => x.Position.x == x1 && x.Position.y == y1);
        endNode = node;
        endNode.Distance = 0;
        endNode.Visited = NODE_END;
        // Ajout d'un node en neighbour plante , penser � d�commenter pathfind pour continuer

    }

    private static void SetUnvisitedList()
    {
        foreach (var node in mapGrid)
        {
            if (node.Visited == NODE_UNVISITED)
            {
                unvisitedNodeList.Add(node);
            }

        }
    }

    class Dijkstra
    {

        public static string[] FindShortestPath()
        {
            do
            {
                var tentativeDistance = currentNode.Distance + 1;
                //Console.Error.WriteLine("count unvisited ");
                foreach (var neighbour in currentNode.Neighbours)
                {
                    if (neighbour.Visited == NODE_UNVISITED)
                    {
                        if (neighbour.Distance > tentativeDistance)
                        {
                            neighbour.Distance = tentativeDistance;
                            neighbour.ShortestPath = currentNode.ShortestPath + " " + currentNode.Name;
                            //Console.Error.WriteLine(neighbour.Distance);
                        }
                    }
                }

                currentNode.Visited = NODE_VISITED;
                unvisitedNodeList.Remove(currentNode);
                if (currentNode.Name == endNode.Name)
                    break;

                currentNode = unvisitedNodeList.OrderBy(x => x.Distance).FirstOrDefault();
            }
            while (currentNode != null && currentNode.Distance != int.MaxValue);


            //Console.Error.WriteLine(currentNode.Distance != int.MaxValue);
            if (endNode.Distance == int.MaxValue)
                return "no path to this way exist".TrimStart().Split(' '); // No path to this gateway exists
            else
                //Console.Error.WriteLine(endNode.ToString());
                return (currentNode.ShortestPath + " " + currentNode.Name).TrimStart().Split(' ');
        }

    }
    #endregion

    #region A*

    public static void initAStar()
    {
        aStarOpenList.Clear();
        aStarCloseList.Clear();
        aStarOpenList.Add(startNode);
        currentNode = startNode;
    }

    public static bool FindShortestPathAstar()
    {

        do
        {
            //Console.Error.WriteLine(aStarOpenList.Count);
            currentNode = aStarOpenList[0];
            aStarOpenList.Remove(currentNode);
            aStarCloseList.Add(currentNode);

            if (currentNode.Name == endNode.Name)
            {
                //Console.Error.WriteLine("PathFind");
                currentNode.ShortestPath = currentNode.ShortestPath + " " + currentNode.Name;
                continue; //Console.Error.WriteLine("PathFind");
            }

            var tentativeDistance = currentNode.Distance + 1;

            foreach (var neighbour in currentNode.Neighbours)
            {

                Node neighbourInClosedList = aStarCloseList.Find(x => x.Name == neighbour.Name);
                if (neighbourInClosedList != null || neighbour.Neighbours.Count == 0)
                {
                    // Skip this neighbour
                    continue;
                }

                tentativeDistance = tentativeDistance + DistEndPoint(neighbour.Position.x, neighbour.Position.y);
                //Console.Error.WriteLine("tentative distance");
                //Console.Error.WriteLine(tentativeDistance);
                Node neighbourInOpenList = aStarOpenList.Find(x => x.Name == neighbour.Name);
                //Console.Error.WriteLine("neighbourinopenlist");
                //Console.Error.WriteLine(neighbourInOpenList);
                //if new path to neighbour is shorter OR neighbour is not in OPEN
                // 58              
                if (tentativeDistance > neighbour.Distance || neighbourInOpenList == null)
                {
                    neighbour.Distance = tentativeDistance;
                    //Console.Error.WriteLine("neighbour.Distance");
                    //Console.Error.WriteLine(neighbour.Distance);
                    // set parent of neighbour to current 
                    neighbour.ShortestPath = currentNode.ShortestPath + " " + currentNode.Name;
                    if (neighbourInOpenList == null)
                    {
                        aStarOpenList.Add(neighbour);
                    }

                    //Console.Error.WriteLine("openlist");
                    //Console.Error.WriteLine(aStarOpenList.Count);
                }
            }

            currentNode.Visited = NODE_VISITED;
            //Console.Error.WriteLine("closelist");
            //Console.Error.WriteLine(aStarCloseList.Count);
        } while (aStarOpenList.Count != 0);


        //Console.Error.WriteLine(aStarOpenList.Count);
        return true;
    }

    private static int DistEndPoint(int x, int y)
    {
        int endPointDistance = (x - endNode.Position.x) * (x - endNode.Position.x) + (y - endNode.Position.y) * (y - endNode.Position.y);
        //Console.Error.WriteLine("distance calculée");
        //Console.Error.WriteLine(endPointDistance);
        return endPointDistance;
    }

    #endregion

    private static void PrepareToRemove(int x1, int y1)
    {
        removeNode.Add("X" + x1 + "Y" + y1);
    }

    private static void RemoveColoriseNode(int x0, int y0)
    {
        Node removeThisNode = mapGrid.Find(x => x.Position.x == x0 && x.Position.y == y0);

        mapGrid.Remove(removeThisNode);
    }

    private static void CreateLinkToNode()
    {
        foreach (Node e in mapGrid)
        {
            /* reset node */
            e.Links.Clear();
            e.Neighbours.Clear();
            e.ShortestPath = "";
            //  Int32.MaxValue pour dijkstra et 0 pour A* 
            e.Distance = Int32.MaxValue; // Dijkstra
            //e.Distance = 0; // A*
            e.Neighbours = new List<Node>();
            e.Visited = NODE_UNVISITED;

            // create link up
            //var linkAvailable =
            //    from node in mapGrid
            //    where (e.Position.x + 1 == node.Position.x && node.Position.y == e.Position.y)
            //          || (e.Position.x - 1 == node.Position.x && node.Position.y == e.Position.y)
            //          || (e.Position.x == node.Position.x && node.Position.y == e.Position.y + 1)
            //          || (e.Position.x == node.Position.x && node.Position.y == e.Position.y - 1)
            //    select new { Name = node.Name, NodeX = node.Position.x, NodeY = node.Position.y };



            //foreach (var link in linkAvailable)
            //{
            //    bool containsItem = links.Any((item => (item.A == link.Name && item.B == e.Name)));
            //    if (link.Name != null && !containsItem)
            //    {
            //        links.Add(new Link(link.Name, e.Name));
            //        e.Links.Add(new Link(link.Name, e.Name));
            //    }
            //}

            foreach (var dir in dicDir)
            {
                int posX = e.Position.x + dir.Value.x;
                int posY = e.Position.y + dir.Value.y;
                string neighbourName = "X" + posX.ToString() + "Y" + posY.ToString();
                Node neighbour = mapGrid.Find(x => x.Name == neighbourName);
                if (neighbour != null)
                {
                    e.Neighbours.Add(neighbour);
                    links.Add(new Link(neighbour.Name, e.Name));
                    e.Links.Add(new Link(neighbour.Name, e.Name));
                }
            }



            //foreach (var oneLink in e.Links)
            //{
            //    // Link A = TARGET / Link B = SOURCE
            //    e.Neighbours.Add(mapGrid.Find(x => x.Name == oneLink.A));
            //    //Console.Error.WriteLine(oneLink);
            //}


            //Console.Error.WriteLine(e.ShortestPath.Length);
            //Console.Error.WriteLine("\n\n");

        }
    }

    public class Coord
    {
        public int x { get; set; }
        public int y { get; set; }

        public Coord(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }



    public class Link
    {
        public string A { get; set; }
        public string B { get; set; }
        public string[] Nodes { get { return new[] { A, B }; } }

        public override string ToString()
        {
            return A + " " + B;
        }
        public Link(string A, string B)
        {
            this.A = A;
            this.B = B;
        }
    }

    public class Node
    {
        public string Name { get; set; }
        public Coord Position { get; set; }
        public List<Link> Links { get; set; }
        public int Distance { get; set; }
        public string Visited { get; set; }
        public List<Node> Neighbours { get; set; }
        public string ShortestPath { get; set; }

        public Node(string name, Coord position, List<Link> links, string visited, List<Node> neighbours)
        {
            this.Name = name;
            this.Position = position;
            this.Links = links;
            //this.Distance = Int32.MaxValue;
            this.Distance = 0;
            this.Visited = visited;
            this.Neighbours = neighbours;
        }

        public override string ToString()
        {
            return string.Format("{0} ({1} pts), neighbours {2}, visited {3}", Name, Distance, Neighbours.Count, Visited);
        }
    }
}